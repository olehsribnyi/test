package com.epam.rd.dockertest.model;


import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "employee")
@Builder
public class Employee {
    @Id
    private Long id;
    private String name;
    private int age;
    private double salary;
}
