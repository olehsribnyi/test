package com.epam.rd.dockertest.controller;


import com.epam.rd.dockertest.model.Department;
import com.epam.rd.dockertest.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/department")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;



    @GetMapping("/all")
    public List<Department> getAll() {
        return departmentService.getAll();
    }

    @GetMapping("/")
    String empty(){
        return "home";
    }

    @GetMapping("/find/{id}")
    public Department get(@PathVariable Long id){
        return departmentService.getById(id);
    }

}
