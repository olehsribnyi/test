package com.epam.rd.dockertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class DockertestApplication {
	public static void main(String[] args) {
		SpringApplication.run(DockertestApplication.class, args);
	}
}
