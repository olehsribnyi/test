package com.epam.rd.dockertest.service;

import com.epam.rd.dockertest.model.Department;
import com.epam.rd.dockertest.repo.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    DepartmentRepository repository;

    @Override
    public Department save(Department dep) {
        repository.save(dep);
        return repository.findById(dep.getId()).get();
    }

    @Override
    public Department getById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public List<Department> getAll() {
        return repository.findAll();
    }

    @Override
    public List<Department> saveAll(List<Department> departments) {
        return repository.saveAll(departments);
    }

    @Override
    public Department update(Department newDep) {
        Department department = repository.findById(newDep.getId()).get();
        updateInstance(department , newDep);
        return repository.save(department);
    }

    @Override
    public void delete(Long id) {
         repository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();

    }

    private Department updateInstance(Department department , Department newDep){
        department.setName(newDep.getName());
        department.setDescription(newDep.getDescription());
        department.setEmployees(newDep.getEmployees());
        return department;
    }



}
