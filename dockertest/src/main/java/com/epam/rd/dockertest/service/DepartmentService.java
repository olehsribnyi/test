package com.epam.rd.dockertest.service;

import com.epam.rd.dockertest.model.Department;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DepartmentService {
    Department save(Department dep);
    Department getById(Long id);
    List<Department> getAll();
    List<Department> saveAll(List<Department> departments);
    Department update(Department newDep);
    void delete(Long id);
    void deleteAll();
}
