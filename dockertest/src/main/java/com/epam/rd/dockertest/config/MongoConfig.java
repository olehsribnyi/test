package com.epam.rd.dockertest.config;

import com.epam.rd.dockertest.repo.DepartmentRepository;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableAutoConfiguration
@EnableMongoRepositories(basePackageClasses = DepartmentRepository.class)
public class MongoConfig {

}
