package com.epam.rd.dockertest.service;

import com.epam.rd.dockertest.Util.BaseTestSpring;
import com.epam.rd.dockertest.Util.DepartmentTestDataGenerator;
import com.epam.rd.dockertest.model.Department;
import org.junit.After;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;



@SpringBootTest(classes = BaseTestSpring.class)
//@ContextConfiguration( classes = BaseTestSpring.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
class DepartmentServiceImplTest {

    @Autowired
    private DepartmentService departmentService;

    @After
    public void cleandb(){
        departmentService.deleteAll();
    }

    @Test
    void shouldSaveIntoDBAndGetItBack() {
        //given
        Department expected = DepartmentTestDataGenerator.generateDepartmentList(1).get(0);
        //when
        Department result =  departmentService.save(expected);
        //then
        assertEquals(expected.getId() , result.getId());
        assertEquals(expected.getName() , result.getName());
        assertEquals(expected.getDescription() , result.getDescription());

        expected.getEmployees().forEach(
                employee-> assertThat(result.getEmployees()  , hasItem(allOf(
                        hasProperty("name" , is(employee.getName())),
                        hasProperty("salary" , is(employee.getSalary()))
                )))
        );

    }

    @Test
    void testGetByIdShouldReturnSavedVar() {
        //given
        Department expected = DepartmentTestDataGenerator.generateDepartmentList(1).get(0);
        departmentService.save(expected);
        //when
        Department result = departmentService.getById(expected.getId());
        //then
        assertEquals(expected.getId() , result.getId());
        assertEquals(expected.getName() , result.getName());
        assertEquals(expected.getDescription() , result.getDescription());

        expected.getEmployees().forEach(
                employee-> assertThat(result.getEmployees()  , hasItem(allOf(
                        hasProperty("name" , is(employee.getName())),
                        hasProperty("salary" , is(employee.getSalary()))
                )))
        );
    }

    @Test
    void testGetAllShouldReturnAllVariablesFromDb() {
        //given
        List<Department> departmentList = DepartmentTestDataGenerator.generateDepartmentList(5);
        departmentService.saveAll(departmentList);
        //when
        List<Department> result = departmentService.getAll();
        //then
        departmentList.forEach(
                dep-> assertThat(result , hasItem(allOf(
                        hasProperty("id" , is(dep.getId())),
                        hasProperty("name" , is(dep.getName()))
                )))
        );
    }

    @Test
    void testUpdateShouldUpdateSavedItem() {
//        //given
//        Department expected = DepartmentTestDataGenerator.generateDepartmentList(1).get(0);
//        departmentService.save(expected);
//        expected.setName("newName");
//        expected.setDescription("New Descr");
//        expected.getEmployees().get(0).setId(121L);
//        expected.getEmployees().get(0).setName("newName");
//        //when
//        departmentService.update(expected);
//        Department result = departmentService.getById(expected.getId());
//        //then
//        expected.getEmployees().forEach(
//                employee-> assertThat(result.getEmployees()  , hasItem(allOf(
//                        hasProperty("id" , is(employee.getId())),
//                        hasProperty("salary" , is(employee.getSalary()))
//                )))
//        );
    }

    @Test
    void testDeleteShouldDeleteFromDelete() {
//        //given
        Department expected = DepartmentTestDataGenerator.generateDepartmentList(1).get(0);
        departmentService.save(expected);
        //when
        departmentService.delete(expected.getId());
//        //then
////        assertNull(result);
//        assertThat(departmentService.getById(expected.getId()) , is(equalTo(null)));
    }

}