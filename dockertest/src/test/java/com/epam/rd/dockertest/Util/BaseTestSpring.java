package com.epam.rd.dockertest.Util;


import com.epam.rd.dockertest.repo.DepartmentRepository;
import com.epam.rd.dockertest.service.DepartmentService;
import com.epam.rd.dockertest.service.DepartmentServiceImpl;
import com.mongodb.MongoClient;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;


@SpringBootConfiguration
@EnableMongoRepositories(basePackageClasses = DepartmentRepository.class)
@ComponentScan(value = "com.epam.rd.dockertest")
@ActiveProfiles("test")
public class BaseTestSpring {

    private static final String MONGO_DB_URL = "localhost";
    private static final String MONGO_DB_NAME = "embeded_db";

    @Bean
    public  DepartmentService departmentService(){
        return new DepartmentServiceImpl();
    }

    @Bean
    public MongoTemplate mongoTemplate() throws IOException {
        EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
        mongo.setBindIp(MONGO_DB_URL);
        MongoClient mongoClient = mongo.getObject();
        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, MONGO_DB_NAME);
        return mongoTemplate;
    }


}

