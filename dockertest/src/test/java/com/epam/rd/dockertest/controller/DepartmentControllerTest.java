package com.epam.rd.dockertest.controller;

import com.epam.rd.dockertest.Util.DepartmentTestDataGenerator;
import com.epam.rd.dockertest.model.Department;
import com.epam.rd.dockertest.service.DepartmentService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;




@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(value = DepartmentController.class)
@ActiveProfiles("test")
class DepartmentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    DepartmentService mockServ;

    @Test
    void getAll() throws Exception {
        //given
        Department expected = DepartmentTestDataGenerator.generateDepartmentList(1).get(0);
        expected.setId(1l);
        when(mockServ.getById(1l)).thenReturn(expected);
        mockMvc.perform(get("/department/find/1"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id" , is(1)));

    }


}