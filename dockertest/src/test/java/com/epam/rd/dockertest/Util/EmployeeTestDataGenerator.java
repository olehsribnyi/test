package com.epam.rd.dockertest.Util;

import com.epam.rd.dockertest.model.Employee;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@UtilityClass
public class EmployeeTestDataGenerator {

    private final Random random = new Random();

    public static List<Employee> generateEmployee(int count){
        return IntStream.range(0,count)
                .mapToObj(EmployeeTestDataGenerator::createEmployee)
                .collect(Collectors.toList());
    }


    private Employee createEmployee(int count){
        return Employee.builder()
                .age(random.nextInt(66))
                .id(new RandomDataGenerator().nextLong(1 , 1313654132l))
                .name(RandomStringUtils.randomAlphabetic(10))
                .salary(random.nextDouble())
                .build();
    }
}
