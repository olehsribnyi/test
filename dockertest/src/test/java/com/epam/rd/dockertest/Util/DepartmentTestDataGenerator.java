package com.epam.rd.dockertest.Util;

import com.epam.rd.dockertest.model.Department;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@UtilityClass
@ActiveProfiles("test")
public class DepartmentTestDataGenerator {
    private final Random random = new Random();

    public static List<Department> generateDepartmentList(int count){
        return IntStream.range(0,count)
                .mapToObj(DepartmentTestDataGenerator::createDepartment)
                .collect(Collectors.toList());
    }

    private Department createDepartment(int count){
        return Department.builder()
                .id(new RandomDataGenerator().nextLong(1 , 1313654132l))
                .name(RandomStringUtils.random(10))
                .description(RandomStringUtils.random(10))
                .employees(EmployeeTestDataGenerator.generateEmployee(random.nextInt(10)))
                .build();
    }
}
